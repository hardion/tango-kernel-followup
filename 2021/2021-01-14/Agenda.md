# Tango Kernel Follow-up Meeting - 2021/01/14

To be held on 2021/01/14 at 15:00 CET on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-12-10/Minutes.md#summary-of-remaining-actions)
 2. [Tango RFCs](https://github.com/tango-controls/rfc/wiki/Meeting-2021-01-14)
 3. Gitlab migration ([TangoTickets#47](https://github.com/tango-controls/TangoTickets/issues/47))
 4. High priority issues
 5. Conda recipes
 6. Tango Kernel Webinars
 7. AOB
