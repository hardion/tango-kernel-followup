# Tango Kernel Follow-up Meeting

Held on 2021/03/11 at 15:00 CET on Zoom.

**Participants**:

- Alberto Lopez (ALBA)
- Anton Joubert (SARAO)
- Benjamin Bertrand (MaxIV)
- Damien Lacoste (ESRF)
- Lorenzo Pivetta (ELETTRA)
- Michal Liszcz (S2Innovation)
- Piotr Coryl (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Stephane Poirier (Soleil)
- Thomas Juerges (LOFAR)

## Minutes

Benjamin got randomly selected to take notes and write the minutes of this meeting.

### Status of [Actions defined in the previous meetings](../2021-02-25/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**: Contact Geoff to inform him about the webinar and confirm Geoff is available

**Action - Anton**: Prepare first part of the coming Tango Kernel Webinar (31st March 9:30 CEST) on pyTango.

**Action - Benjamin**: Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.
**JacORB is under LGPL: <https://www.jacorb.org/lgpl-explained.html>.**
**Reynald proposed to contact Frederic Picca to get more information about license issue with Debian packages.**

**Action - Benjamin**: Migrate tango-kernel-followup repository
**Done. Branch master should be renamed to main (done since).**

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Reynald**: Contact Igor to coordinate the migration to Gitlab of the repositories he's administrating:
rest-server, rest-test-suite, rest-api-java, tango-camel, tango-controls-demo, jtango-maven-archetype
**E-mail sent. No intention to migrate to GitLab for now. Is it mandatory to move? The consensus is that for consistency every repo should be moved. Will wait for his feedback and see again when all other repos have been moved.**

**Action -Reynald**: Contact his ESRF colleagues and coordinate the migration of the repositories they are currently
administrating (pogo, SettingsManager, atk, matlab-binding, jupyTango, igorpro-binding, labview-binding, JSSHTerminal,
Astor, jive, LogViewer, atk-panel, C_Binding, atk-tuning, ForwardedComposer, wunderground, DBBench)
**Some repos will be migrated soon: atk and atk-panel. Keep action for others.**

**Action - Reynald**:
Share the knowledge, after discussing with Emmanuel Taurel, about how the last Tango Windows distribution was created.
<https://github.com/tango-controls/TangoTickets/issues/37>
Reynald should upload all the useful scripts he found to build the Tango Source Distribution on the Tango Source Distribution repository.
**In Progress. MR created after the meeting: <https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/100>**

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Sergi**: Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to migrated under hdbpp subgroup.

**Action - Sergi**:
cpptango#742 (on names on logs) to be reviewed
**Could not reproduce with pytango. WIP**

**Action - Sergi**: debug a memory leak issue when using pytango 9.3.3 and older when reading states of devices in a thread.
Ongoing, (note: please add a issue link)
**Issue solved**

**Action - Thomas Braun**: Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side.

**Action - Thomas Braun**: Start the migration process for TangoTickets repository.
**Issue created: <https://github.com/tango-controls/TangoTickets/issues/53>**

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help).
**Vincent started to change the links - WIP**

**Action - S2Innovation**: Organize the migration of the repositories they are administrating
(tango-doc, dsc, sys-tango-benchmark, sys-tango-benchmark-standard-tests, dsc-import, tangobox).
**tango-doc and benchmark repos done. tangobox in progress.
dsc and dsc-import: more work needed on the server side application.**

### Tango RFCs

Not much news. Piotr tried to improve the headings. He discussed with Vincent on how to improve the metadata.
Vincent will propose something so that they can be properly converted to HTML on readthedocs.

### Gitlab migration

For JTango Krystian (S2Innovation) is currently working on migrating the CI to gitlab:

- working to have the same release process as on github
- upload to bintray will be replaced with maven central

Alberto is working on a [MR](https://gitlab.com/tango-controls/pytango/-/merge_requests/413) to use Docker images
to improve pytango gitlab-ci pipeline. Alberto proposed to have a separate repo to create the docker images.
Anton would prefer to keep everyting in the same repo.
Python 2.7 and 3.5 support becomes difficult for pytango (conda-forge only officially Python >=3.5).
ALBA still rely on Python 3.5 (Debian 9).
Would not want to have to maintain the old conda recipes on github.
It might be possible to enable python 2.7 on conda-forge.

New MR for pytango is using C++11 syntax: <https://gitlab.com/tango-controls/pytango/-/merge_requests/418>.
Is it OK for everyone to use C++11?
Action Benjamin to check for MAX IV and contact Anton.

### High Priority issues

ESRF encountered a bug that has probably been there for a very long time.
It was discovered on a Device server exporting many devices with many attributes.
After 20 days, the device server is crashing due to a leak in corba smart pointers.
Emmanuel Taurel found the issue: <https://gitlab.com/tango-controls/cppTango/-/merge_requests/842>
Would be nice to have a new release quite soon.

ALBA want to expose server init hook in pytango.
It's only in 9.4 (cpptango). Might be risky in 9.3.
pytango has post init callback that could be used instead.
Michal to check if it could be backported. Issue to be created?
Maybe something can be done in pytango without touching cpptango.
Would need to be exported via boost api anyway.
Original issue: <https://github.com/tango-controls/cppTango/issues/498>

### Tango Kernel Webinars

Andy contacted Geoff today. Reynald will contact Andy.
Decide if we keep the 31st or not early next week (2021/03/15).

### Future of pogo

Pascal Verdier will retire at the end of the month. Damien Lacoste will take over.
Pogo is based on a lot of old and deprecated technolgies.
What should be done?
Damien will prepare and send a survey on tango mailing list.

### AOB

#### posting to tango ML

Lorenzo tried to post on tango info mailing list but the message didn't show up.
Has something changed?
**Action - Reynald**: to contact Jean-Michel

#### HDB++ meeting tomorrow at 11:00 (2021/03/12)

#### conda recipes

CRC checksum problem with cpptango-dbg package solved: <https://github.com/conda-forge/cpptango-feedstock/pull/2>
Tested OK by Reynald. Anton will test with pytango.

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place on Thursday 25th March 2021 at 15:00 **CET**.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**: Contact Geoff to inform him about the webinar and confirm Geoff is available

**Action - Anton**: Prepare first part of the coming Tango Kernel Webinar (31st March 9:30 CEST) on pyTango.

**Action - Benjamin**: Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.
JacORB is under LGPL: <https://www.jacorb.org/lgpl-explained.html>.
Reynald proposed to contact Frederic Picca to get more information about license issue with Debian packages.

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Benjamin B.**:
Check if C++11 can be used at MAX IV and contact Anton.

**Action - Reynald**: Contact Igor to coordinate the migration to Gitlab of the repositories he's administrating:
rest-server, rest-test-suite, rest-api-java, tango-camel, tango-controls-demo, jtango-maven-archetype
E-mail sent. No intention to migrate to GitLab for now. Is it mandatory to move? The consensus is that for consistency every repo should be moved. Will wait for his feedback and see again when all other repos have been moved.

**Action - Reynald**: Contact his ESRF colleagues and coordinate the migration of the repositories they are currently
administrating (pogo, SettingsManager, matlab-binding, jupyTango, igorpro-binding, labview-binding, JSSHTerminal,
Astor, jive, LogViewer, C_Binding, atk-tuning, ForwardedComposer, wunderground, DBBench)

**Action - Reynald**:
Share the knowledge, after discussing with Emmanuel Taurel, about how the last Tango Windows distribution was created.
<https://github.com/tango-controls/TangoTickets/issues/37>
Reynald should upload all the useful scripts he found to build the Tango Source Distribution on the Tango Source Distribution repository.
In Progress. MR created after the meeting: <https://gitlab.com/tango-controls/TangoSourceDistribution/-/merge_requests/100>

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Reynald**:
Contact Frederic Picca to get more information about JacORB license issue with Debian packages.

**Action - Reynald**:
Contact Jean-Michel about mailing list issue

**Action - Sergi**: Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to migrated under hdbpp subgroup.

**Action - Sergi**:
cpptango#742 (on names on logs) to be reviewed
Could not reproduce with pytango - WIP

**Action - Thomas Braun**: Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side.

**Action - Thomas Braun**: Start the migration process for TangoTickets repository.
Issue created: <https://github.com/tango-controls/TangoTickets/issues/53>

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help).

**Action - S2Innovation**: Organize the migration of the repositories they are administrating
(dsc, dsc-import, tangobox).
