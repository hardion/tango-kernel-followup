# Tango Kernel Follow-up Meeting

Held on 2021/04/08 at 15:00 CEST on Zoom.

**Participants**:

- Anton Joubert (SARAO)
- Benjamin Bertrand (MaxIV)
- Jairo Moldes (ALBA)
- Krystian Kedron (S2Innovation)
- Lorenzo Pivetta (ELETTRA)
- Michał Liszcz (S2Innovation)
- Nicolas Leclercq (ESRF)
- Piotr Goryl (S2Innovation)
- Reynald Bourtembourg (ESRF)
- Sergi Rubio (ALBA)
- Sonja Vrcic (SKAO)
- Thomas Braun (byte physics)
- Thomas Juerges (LOFAR)
- Vincent Hardion (MaxIV)

## Minutes

Nicolas has been randomly designated volunteer to take notes and write the minutes of this meeting. 
He asked for some help to Reynald to contribute to these minutes.

### Status of [Actions defined in the previous meetings](../2021-03-25/Minutes.md#summary-of-remaining-actions)

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**:
Contact Geoff to inform him about the webinar and confirm Geoff is available.  
**Geoff cannot make it on 14th April. Anton will contact Geoff and agree on a suitable date for PyTango Kernel Webinar 
and then advertise it on the Tango Mailing list and on tango-controls website. He will contact Reynald in case of 
difficulty publishing on tango-controls.org website**

**Action - Andy (ESRF)**:
Contact freexian about maintaining omniORB packages in debian

**Action - Benjamin B.**:
Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.
JacORB is under LGPL: <https://www.jacorb.org/lgpl-explained.html>.
Might require proprietary license as idl/omg/CosBridgeAdmin.idl is not under LGPL-v2.
**Benjamin created the following issue on conda-forge for JTango: https://github.com/conda-forge/staged-recipes/pull/14433.
He asked a question to know under which licence(s) he should release the package**

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the repositories they are currently
administrating (pogo, SettingsManager, matlab-binding, jupyTango, igorpro-binding, labview-binding, JSSHTerminal,
Astor, jive, LogViewer, C_Binding, atk-tuning, ForwardedComposer, wunderground, DBBench)
**SettingsManager, matlab-binding, jupyTango, igorpro-binding, labview-binding have been migrated to gitlab.**

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Reynald**:
Contact Jean-Michel about mailing list issue. **It seems like the issue with the mailing list is gone.  
Lorenzo managed to send an e-mail on 23rd March.**

**Action - Sergi**:
Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to migrated under hdbpp subgroup. **Sergi will do it in the next months**

**Action - Sergi**:
cpptango#742 (on names on logs) to be reviewed. Michał is reworking the MR in response to Sergi's feedback.
**Nicolas will try to give some answers to Michal's questions if he still remembers enough about log4tango**

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.

**Action - Thomas B.**:
Start the migration process for TangoTickets repository, see [here](https://github.com/tango-controls/TangoTickets/issues/53).
**Done => https://gitlab.com/tango-controls/TangoTickets**

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help). **Benjamin said there will be an internal meeting at Max IV on Friday 9th April on that topic**

**Action - S2Innovation**: Organize the migration of the repositories they are administrating
(dsc, dsc-import, tangobox).
**dsc and dsc-import have been migrated. dsc will be archived only when the app running within the web page will be updated.
Because it currently stores a xmi backup on github).**

### Gitlab Migration

The following repositories have been migrated since the last Tango Kernel Meeting:
- [jupyTango](https://gitlab.com/tango-controls/jupyTango)
- [labview-binding](https://gitlab.com/tango-controls/labview-binding)
- [igorpro-binding](https://gitlab.com/tango-controls/igorpro-binding)
- [matlab-binding](https://gitlab.com/tango-controls/matlab-binding)
- [dsc-import](https://gitlab.com/tango-controls/dsc-import)
- [dsc](https://gitlab.com/tango-controls/dsc)
- [TangoTickets](https://gitlab.com/tango-controls/TangoTickets)

dsc will be archived only when the app running within the web page will be updated because it currently stores a xmi backup on github.

Michal started working on the CI migration of the benchmark related repositories.

Some improvements have been made on tango-doc repository to automatically build the documentation using gitlab-ci when there is a new contribution.

Krystian is still actively working on JTango migration.

#### Bintray sunset

JFrog will be sunsetting Bintray on May 1, 2021.  
Bintray is currently used as Maven repository for many Tango projects using Java and new releases are currently 
automatically deployed to Bintray.  
For JTango, the releases were deployed to Bintray and some releases were mirrored on Maven Central.  
Krystian prepared the new JTango gitlab pipelines to deploy directly on Maven Central instead of Bintray when there is 
a new JTango release.
Krystian is currently working on his fork for the JTango migration. He can provide help to the maintainers of other 
Tango Java projects to help them to adapt the deployment to deploy automatically the new releases on Maven Central instead of Bintray.

**Action - Krystian & Tango Java Projects maintainers**: Adapt mvn release target to deploy new releases on Maven Central instead of Bintray.

### RFC

Please refer to https://github.com/tango-controls/rfc/wiki/Meeting-2021-01-28

Piotr will start working on the RFC related to messages exchanged in Tango, describing the data exchanged during Tango communication.  
He will probably require some help from the experts at some points.  
Vincent proposed on the chat to provide some help, next week.

### High Priority issues 

Anton mentioned these 2 merge requests: 
- https://gitlab.com/tango-controls/pytango/-/merge_requests/416 (get_fqdn)

Conclusion (to be confirmed) was to add a new method with a more significant name, to live with the new method and old method for a while, 
and then eventually remove the badly named one at some point. In any case, please comment the ticket if you have an opinion on that topic.
It is difficult to remove a public method because it is very difficult to know who might be using it.

- https://gitlab.com/tango-controls/pytango/-/merge_requests/421 (set_cvs_tag and set_cvs_location)

Anton would like a test with https://gitlab.com/tango-controls/pytango/-/merge_requests/421.  
Thomas sent the following link to stackoverflow about how to do similar things with git: https://stackoverflow.com/a/16529140  
Anton: "For versioning Python code on MeerKAT we use this library:  https://github.com/ska-sa/katversion.   
Then the package versions matches either the git tag, or gives a git hash, if the checkout is not tagged."

Vincent: "at this moment we use gitlab variable ($CI_PROJECT_URL, …)".

At Elettra, they use the hash of the final object.

**Action - Vincent**: Investigate whether this https://gitlab.com/tango-controls/pytango/-/merge_requests/421 makes sense for Git and if yes, 
create an issue in TangoTickets or relevant repositories to get some new set_vcs_tag aand set_vcs_location methods.

Reynald reported that they encountered again some problems with some clients using an old version of JTango (< 9.6.6) which 
were creating many connections to Tango device servers, until a point where the device server could not open any socket 
any more. This seemed to be a bug in the reconnection of JTango when a host network alias name is used instead of the 
real host name in the TANGO_HOST variable (bug not present in JTango >= 9.6.6). lsof helped to find the clients causing some troubles.

Reynald reported an issue they encountered due to the fact that Pogo was generating files in latin-1 encoding whereas gcc assumes UTF-8 now by default.  
This was causing some Unknown CORBA exceptions when updating the attribute configuration of an attribute having an unsual 
character in the unit (like the degree sign for instance). Changing Pogo startup script to use "-Dfile.encoding=UTF-8" 
option and recompiling the device server solved this issue in the ESRF use case.  
Another option would have been to use "-finput-charset=ISO-8859-1" gcc option.  
This is eventually something which could be managed by Pogo to generate Makefiles using that option.  
ESRF will try to use a Database server with a schema using UTF-8 instead of latin-1 in the coming months.  
Thomas B. said that if we decide to change the default and switch to UTF-8, we should implement the change at the same 
time on every affected core components.

**Action - Reynald**: Create an issue in Pogo to propose an optional Character Set Encoding for the generated files.

### Tango Kernel Webinars

Geoff cannot make it on 14th April. Anton will contact Geoff and agree on a suitable date for PyTango Kernel Webinar.

Reynald uploaded 2 videos for Tango Kernel Webinar#2 (Pogo and astor/starter) on [tango-controls youtube channel](https://www.youtube.com/channel/UCezS9cMkektZNItYnPOAQvg).

Videos for Tango Kernel Webinar#3 will be uploaded soon.

### AOB
#### docker-mysql
Benjamin will update the pipelines in docker-mysql repository to deploy the docker on gitlab infrastructure instead of docker hub.  
This should simplify the usage of this docker image from gitlab-ci.

#### Next teleconf meeting

Tango Kernel Teleconf Meetings take place on the 2nd and 4th Thursday of each month, at 15:00 CET or CEST (Paris time).

So the next Tango Kernel Teleconf Meeting will take place on Thursday 22nd April 2021 at 15:00 **CEST**.

## Summary of remaining actions

**Action - All institutes**:
Please vote (thumb up reaction in the description of the issue, or e-mail) for the most critical issues you would like to get solved first.

**Action - Andy (ESRF)**:
Update the description of issue <https://github.com/tango-controls/cppTango/issues/822>

**Action - Andy (ESRF)**:
Update Documentation to give a recipe on how to install Tango with the Tango Source Distribution on Ubuntu

**Action - Andy (ESRF)**:
Contact freexian about maintaining omniORB packages in debian

**Action - Anton (SRAO)**: Contact Geoff and agree on a suitable date for PyTango Kernel Webinar 
and then advertise it on the Tango Mailing list and on tango-controls website.

**Action - Benjamin B.**:
Investigate on whether it would be possible to create conda packages for the Tango java tools.
Need to check whether the JacORB license and java CORBA classes allow to redistribute freely the software.
JacORB is under LGPL: <https://www.jacorb.org/lgpl-explained.html>.
Might require proprietary license as idl/omg/CosBridgeAdmin.idl is not under LGPL-v2.
**Benjamin created the following issue on conda-forge for JTango: https://github.com/conda-forge/staged-recipes/pull/14433.
He asked a question to know under which licence(s) he should release the package**

**Action - Benjamin B.**:
Try out gitlab runners on windows for pytango

**Action - Krystian & Tango Java Projects maintainers**: Adapt mvn release target to deploy new releases on Maven Central instead of Bintray.

**Action - Nicolas L.(ESRF)**: Comment https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 and try to answer to Michał's questions

**Action - Reynald**:
Contact his ESRF colleagues and coordinate the migration of the remaining repositories they are currently
administrating (pogo, JSSHTerminal, Astor, jive, LogViewer, C_Binding, atk-tuning, ForwardedComposer, wunderground, DBBench)

**Action - Reynald**:
Look at old cppTango PRs targeting `tango-v10`, and comment/close/create issue

**Action - Reynald**: Create an issue in Pogo to propose an optional Character Set Encoding for the generated files.

**Action - Sergi**:
Organize the migration of the repositories he's administrating (fandango, PANIC, simulatorDS, VACCA).
PyTangoArchiving will have to migrated under hdbpp subgroup.

**Action - Sergi**:
https://gitlab.com/tango-controls/cppTango/-/merge_requests/742 (on names on logs) to be reviewed. Michał is reworking the MR in response to Sergi's feedback.

**Action - Thomas B.**:
Try to reproduce the issues pytango#409 and pytango#371 and try to identify the origin of the
problem (pytango or cpptango?) and to find a solution to avoid the segmentation fault on the client side. pytango#409 is a
cppTango bug, needs a proper fix.

**Action - Thomas B.**:
Try out the gitlab runners on windows which are in beta phase from gitlab

**Action - Vincent**: Ensure rest-api repository can be migrated smoothly and coordinate the migration with the Gitlab
migration team (Benjamin could help).

**Action - Vincent**: Investigate whether https://gitlab.com/tango-controls/pytango/-/merge_requests/421 makes sense for Git and if yes, 
create an issue in TangoTickets or relevant repositories to get some new set_vcs_tag aand set_vcs_location methods.

**Action - S2Innovation**: Organize the migration of the repositories they are administrating
(dsc, tangobox, benchmarks).

