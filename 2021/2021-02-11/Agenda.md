# Tango Kernel Follow-up Meeting - 2021/02/11

To be held on 2021/02/11 at 15:00 CET on Zoom.

# Agenda
 
 1. Kernel Teleconf Minutes Edition Rotation
 2. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2021/2021-01-28/Minutes.md#summary-of-remaining-actions)
 3. Gitlab migration ([TangoTickets#47](https://github.com/tango-controls/TangoTickets/issues/47))
 4. [Tango RFCs](https://github.com/tango-controls/rfc/wiki/Meeting-2021-02-11)
 5. cppTango: Is it still desired that we actively manage two branches: 9.3-backports and tango-9-lts? The current approach is that both are managed and 9.3-backports would also be the source of 9.3.5 which is just 9.3.4 with fixes.
 6. High priority issues
 7. Tango Kernel Webinars
 8. AOB
