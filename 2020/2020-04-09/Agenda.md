# Tango Kernel Follow-up Meeting - 2020/04/09

To be held on 2020/04/09 at 15:00 CEST on Zoom.

# Agenda
 
 1. Tango Source Distribution News
 2. cppTango News
 3. JTango News
 4. PyTango News
 5. Conda packages
 6. Tango Meeting 2020
 7. High priority issues
 8. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-03-26/Minutes.md#summary-of-remaining-actions)
 9. AOB
     - HDB++
     - omniORB 4.3.0 beta 1
     - OpenTracing
 