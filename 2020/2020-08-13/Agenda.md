# Tango Kernel Follow-up Meeting - 2020/08/13

To be held on 2020/08/13 at 15:00 CEST on Zoom.

# Agenda
 
 1. Status of [Actions defined in the previous meetings](https://github.com/tango-controls/tango-kernel-followup/blob/master/2020/2020-07-09/Minutes.md#summary-of-remaining-actions)
 2. Tango Kernel training
 3. cppTango News
 4. JTango News
 5. PyTango News
 6. Tango Source Distribution News
 7. Conda packages
 8. High priority issues
 9. AOB
